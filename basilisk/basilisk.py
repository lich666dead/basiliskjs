import os
import json


class PhantomJS:


    @classmethod
    def get(
            cls, url = None, js = None, screenshot = False, image_name = 'Basilisk',
            content = False, get_url = False, get_cookies = False, userAgent = '',
            loadImages = False, param = None, command = 'phantomjs ', core = True
    ):
        if param is not None:
            try:
                cls.__slots__ = param
                param.clear()
                cls().writeNode()
                return cls().__run(command)
            except Exception:
                raise KeyError
        else:
            ImagesSize = {'width': 1920, 'height': 1080}
            cls.__slots__ = locals()
            cls.__slots__.pop('cls')
            cls().writeNode()
            return cls().__run(command)

    @staticmethod
    def __necessary():
        return '''var webPage = require('webpage');
var page = webPage.create();var data = {};'''

    @staticmethod
    def __settings(*args):
        return '''page.settings.loadImages=%s;
page.viewportSize=%s;page.settings.userAgent='%s';''' % args

    @staticmethod
    def __page(url):
        return '''page.open( '%s' , function(status) {data['status'] = status;});''' % url

    @staticmethod
    def __onLoadFinished(*args):
        temp = ''
        for i in args:
            temp += '%s\n' % i
        return '''page.onLoadFinished = function() {%sconsole.log( JSON.stringify(data));phantom.exit();};''' % temp

    @staticmethod
    def __jsAvalute(js):
        if type(js) is list:
            return '''var temp = {};
            var js = %s;
                    for (var i = 0; i != js.length; i++) {
                        try {
                              temp[i] = page.evaluateJavaScript("function(){ return " + js[i] + "; }");
                        } catch (e) {
                            temp[i] = e;
                        }
                    }; data['js'] = temp;''' % str(js)
        elif type(js) is str:
            return '''var temp = page.evaluate(function() {
              %s
        });data['js'] = temp;''' % js
        else:
            return ''

    @staticmethod
    def __get_cookies(cookies):
        if cookies:
            return "data['cookies'] = page.cookies;"
        else:
            return ''

    @staticmethod
    def __screenshot(screenshot, imageName):
        if screenshot:
            return '''page.render( '%s.png');''' % imageName
        else:
            return ''

    @staticmethod
    def __content(content):
        if content:
            return "data['content'] = page.content;"
        else:
            return ''

    @staticmethod
    def __pageUrl(get_url):
        if get_url:
            return "data['page_url'] = page.url;"
        else:
            return ''

    def writeNode(self):
        if self.__slots__['core']:
            with open('run.js', 'w') as node:
                node.write(self.__necessary() +
                           self.__settings('true' if self.__slots__['loadImages'] else 'false',
                                           self.__slots__['ImagesSize'].__str__(),
                                           self.__slots__['userAgent']) +
                           self.__page(self.__slots__['url']) +
                           self.__onLoadFinished(
                               self.__jsAvalute(self.__slots__['js']),
                               self.__get_cookies(self.__slots__['get_cookies']),
                               self.__screenshot(self.__slots__['screenshot'],
                                                 self.__slots__['image_name']),
                               self.__content(self.__slots__['content']),
                               self.__pageUrl(self.__slots__['get_url']),
                           ))

    @staticmethod
    def __run(command):
        with os.popen(command + ' run.js') as j:
            for i in j.read().split('\n'):
                try:
                    return json.loads(i)
                except:
                    continue
