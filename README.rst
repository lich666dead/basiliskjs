
BasiliskJS - Scriptable Headless WebKit
=========================

`BasiliskJS <https://pypi.python.org/pypi/BasiliskJS>`_ Представляет собой WebKit для python, основан на `PhantomJS <http://phantomjs.org>`_ .

Возможность
============

- **Быстрое тестирование**. Возможность быстрого тестирования без браузера!
- **Автоматизация dom**. Простой интерфейс.
- **Работа с js**. Есть возможность выполнять JavaScript.
- **Захват экрана**. Возможность сделать снимок страницы любого размера.


Пример работы
-------------
Простой запрос на http://phantomjs.org/.

.. code-block:: python

    >>> from basilisk import PhantomJS
    >>> PhantomJS.get('http://phantomjs.org/')

Запрос с выполнением js.

.. code-block:: python

    from basilisk import PhantomJS
    js = '''
    var temp = {};
    for (var i = 0; i != document.getElementsByClassName('nav-item-name').length; i++) {
        temp[i] = document.getElementsByClassName('nav-item-name')[i].innerText;
     }
     return temp;
     '''
     data = {
         'loadImages': False,
         'get_cookies': False,
         'get_url': False,
         'content': False,
         'image_name': 'BasiliskJS',
         'screenshot': False,
         'js': js,
         'ImagesSize':{'width':1920, 'height':1080},
         'userAgent':''
         }
     print(PhantomJS.get('http://phantomjs.org/documentation/', param=data))


Простой запрос с простым выполнением js

.. code-block:: python

    from basilisk import PhantomJS

    js = [
        "document.getElementsByClassName('explanation')[0].innerText"
        ]

    PhantomJS.get('http://phantomjs.org/', js = js)



Показать html контент

.. code-block:: python

    >>> from basilisk import PhantomJS
    >>> PhantomJS.get('http://phantomjs.org/', content=True)



Параметры метода get()
-------------    
- **url**. - url для get запроса.
- **content**. - Паказать content, по умолчанию( False ).
- **load_image**. - Загрузка изаброжений сайта, по умолчанию( False ).
- **userAgent**. - User-Agent, по умолчанию( "BasiliskJS" ).
- **js**. - Выполнить полноценый js.
- **get_url**. - Показать url, по умолчанию( False ).
- **screenshot**. - Сделать скриншот сайта, по умолчанию( False ).
- **get_cookies**. - Получить cookies по умолчанию( False ).
- **ImagesSize**. - Размер скриншота по умолчанию( {'width':1920, 'height':1080} ).
- **image_name**. - Название файла скриншот или полный путь к файлу , по умолчанию( BasiliskJS ).
- **command**. - Для работы требуется браузер PhantomJS, параметр отвечает за путь к нему, по умолчанию( phantomjs ).

Развитие
-------------   
На данный момент мы на стадии Pre-Alpha. Вы можете увидеть сообщения об ошибках и т.д. 
    